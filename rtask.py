#!/usr/bin/env python3
# 2019  Bogdan Lochinski

import argparse
import requests
import json
from pyasn1.compat.octets import null
from collections import Counter
from collections import OrderedDict

parser = argparse.ArgumentParser()
parser.add_argument("user", help="bitbucket.org user", type=str)
parser.add_argument("TOP", help="TOP most commiters", type=int)
parser.add_argument("LIMIT", help="Analyse only last <LIMIT> commits ", type=int)
args = parser.parse_args()

user = args.user
TOP = args.TOP
LIMIT = args.LIMIT

print('Bitbucket.org REST API demo script.')
print('Show TOP ' + str(TOP) + ' comitters from all \'' + user + '\' repos')
# Collect Repositories using bitbucket.org REST API
url = 'https://api.bitbucket.org/2.0/users/' + user + '/repositories'
headers = {'Content-Type': 'application/json'}
repo_list = []
while url:
    r = requests.get(url, headers=headers).json()
    repos = r['values']
    for repo in repos:
        repo_list.append(repo['slug'])
# Using "slug" instead of "name"
    if 'next' in r:
        url = r['next']
    else:
        url = null
# Collect Commits using bitbucket.org REST API


def get_commmits_from_repo(repo, user, limit):
    url = 'https://api.bitbucket.org/2.0/repositories/' + user + '/' + repo + '/commits'
    headers = {'Content-Type': 'application/json'}
    commiters = []
    commits = []

    while url:
        r = requests.get(url, headers=headers).json()
        commits_on_page = r['values']
        for commit in commits_on_page:
            commits.append(commit)
            if len(commits) >= limit:
                break
        if 'next' in r:
            url = r['next']
        else:
            url = null
        if len(commits) >= limit:
            break

    for commit in commits:
        if 'author' in commit:
            commiters.append(commit['author']['raw'])
# Optionaly you can user Display Name, but sometimes it's not used
#      commit['author']['user']['display_name']

    return dict(Counter(commiters))


# GET 'limited' commits from repo
all_repos = {}
for repo in repo_list:
    comm_count_in_repo = get_commmits_from_repo(repo, user, LIMIT)
    out_dict = {repo: comm_count_in_repo}
    all_repos.update(out_dict)

total_commits = {}
glb_sum = 0

# Select same authors form different repos
for repo, commiters in all_repos.items():
    for autor, commits_sum in commiters.items():
        glb_sum += commits_sum
        if autor not in total_commits:
            total_commits.update({autor: {'git_repos': {repo: commits_sum}, 'total_commits_share_percentage': commits_sum}})
        else:
            total_commits[autor]['git_repos'].update({repo: commits_sum})
            total_commits[autor]['total_commits_share_percentage'] += commits_sum

# Calculate percentage for each author
helper_list_with_percent = {}
for autor, contrib in total_commits.items():
    autor_sum = contrib['total_commits_share_percentage']
    share_percentage = (autor_sum/glb_sum)*100
    total_commits[autor]['total_commits_share_percentage'] = share_percentage
    helper_list_with_percent.update({autor: share_percentage})
# Sort by percentage
percent_sorted = OrderedDict(sorted(helper_list_with_percent.items(), key=lambda t: t[1], reverse=True))
# Cut TOP
top_most = {k: percent_sorted[k] for k in list(percent_sorted)[:TOP]}
# Somehow cutting loses sorting, need to sort again
top_most_sorted = OrderedDict(sorted(top_most.items(), key=lambda t: t[1], reverse=True))

# Get full data
top_commiters = {}
for autor in top_most_sorted:
    top_commiters.update({autor: total_commits[autor]})

out_dict = {user: top_commiters}

# final output
# to console
output_json = json.dumps(out_dict, indent=4)
print(output_json)


